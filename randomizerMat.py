from random import *

def probaOfOccuranceOfOne(proba):
    probaMaker=[i for i in range(101)]
    randomProba=choice(probaMaker)
    if proba >= randomProba :
        return 1
    return 0


def createMatriceOfRandomOneElementSquare(dim,proba):
    mat = []
    for i in range(dim):
        rowList = []
        for j in range(dim):           
            rowList.append([probaOfOccuranceOfOne(proba) for i in range(1)][0])
        mat.append(rowList)

    return mat

def createMatrixOfRandomOneElement(rowCount, colCount, proba):
    mat = []
    for i in range(rowCount):
        rowList = []
        for j in range(colCount):
            rowList.append([probaOfOccuranceOfOne(proba) for i in range(1)][0])
        mat.append(rowList)

    return mat
