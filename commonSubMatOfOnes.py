from random import *
import randomizerMat

def probaOfOccuranceOfOne(proba):
    probaMaker=[i for i in range(101)]
    randomProba=choice(probaMaker)
    if proba >= randomProba :
        return 1
    return 0

def createMatriceOfOneElementSquare(dim,element):
    mat = []
    for i in range(dim):
        rowList = []
        for j in range(dim):           
            rowList.append(element)
        mat.append(rowList)

    return mat

def createMatrixOfOneElement(rowCount, colCount, element):
    mat = []
    for i in range(rowCount):
        rowList = []
        for j in range(colCount):
            rowList.append(element)
        mat.append(rowList)

    return mat

def matriceDim(matrice):
    rowDim = len(matrice)
    colDim = len(matrice[0])
    if colDim<rowDim:
        return colDim , rowDim
    return rowDim , colDim

def minDim(matrice):
    return matriceDim(matrice)[0]

def subMatSquare(matrice,start,dim):    
    mySubMatSquare=[]
    i=0
    while i < dim :
        mySubMatSquare.append(matrice[start[0]+i][start[1]:start[1]+dim])
        #print(mySubMatSquare)
        i += 1
    return mySubMatSquare


def matElementProduit(matrice1,matrice2):


    rowDimMat=len(matrice1)
    colDimMat=len(matrice1[0])   

    result = createMatrixOfOneElement(rowDimMat, colDimMat, 0)
    for i in range(rowDimMat):  
        for j in range(colDimMat):
            result[i][j] = matrice1[i][j] * matrice2[i][j]
    for r in result:
        #print(r)
        return result



def inMat(matrice1,matrice2):

    rowDimMat=len(matrice1)
    colDimMat=len(matrice1[0])
    rowDimMat2=len(matrice2)
    colDimMat2=len(matrice2[0])
    
    if (rowDimMat != rowDimMat2) or (colDimMat != colDimMat2):
        return "matrice1 and matrice2 should have the same dim"

    prodOfMat = matElementProduit(matrice1,matrice2)
    rowDimOfMat = len(prodOfMat)
    colDimOfMat = len(prodOfMat[0])

    
    limitOfStart = rowDimOfMat - colDimOfMat
    minDimOfMat = minDim(prodOfMat) #modifialble dans while
    start = [0,0] #modifiable dans while
    myCounter = 0 #modifiable dans while
  

    matZero = createMatrixOfOneElement(rowDimOfMat, colDimOfMat, 0)
    if prodOfMat == matZero :
        return 0

    

    mySubMatSquare = subMatSquare(prodOfMat,start,minDimOfMat)
    matToLookLike = createMatriceOfOneElementSquare(minDimOfMat,1)
        
    #print(prodOfMat)
    #print(minDimOfMat)
    stoper = False
    while stoper == False :
         
        if limitOfStart > 0:
            start=[0,0]

            for i in range(limitOfStart+myCounter+1):
                start[0] = i                
                for j in range(myCounter+1):
                    start[1] = j                       
                    mySubMatSquare = subMatSquare(prodOfMat,start,minDimOfMat)
                    #print("starting point : ",start)
                    #print("sub mat is : ",mySubMatSquare)
                    #print("to look like : ",matToLookLike)

                    if mySubMatSquare == matToLookLike :
                        stoper = True
                        return minDimOfMat                      
                        
                        
                        

            minDimOfMat -= 1            
            myCounter += 1
            matToLookLike = createMatriceOfOneElementSquare(minDimOfMat,1)

        if limitOfStart <0 :
            start=[0,0]

            for i in range(myCounter+1):
                start[0] = i                
                for j in range(abs(limitOfStart) + myCounter+1):
                    start[1] = j                       
                    mySubMatSquare = subMatSquare(prodOfMat,start,minDimOfMat)
                    #print("starting point : ",start)
                    #print("sub mat is : ",mySubMatSquare)
                    #print("to look like : ",matToLookLike)

                    if mySubMatSquare == matToLookLike :
                        stoper = True
                        return minDimOfMat                      
                        
                        
                        

            minDimOfMat -= 1            
            myCounter += 1
            matToLookLike = createMatriceOfOneElementSquare(minDimOfMat,1)

            

  




Tm1=[[1,0,1,0],
    [1,1,1,1],
    [1,1,1,1],
    [1,1,1,1]]  

Tm2=[[0,0,0,0],
    [1,1,1,1],
    [1,1,1,0],
    [1,1,1,1]] 


#print(inMat(randomizerMat.createMatrixOfRandomOneElement(6, 7, 100),randomizerMat.createMatrixOfRandomOneElement(6, 7, 100)))
